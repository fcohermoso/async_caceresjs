# Ejemplos async #

Este repo contiene varios ejemplos cómo se implementan llamadas asíncronas en Javascript

### ¿Qué necesito para ejecutar los ejemplos? ###
* Visual Studio Code
* NodeJS versión 8 o superior
* Docker
* Mongo en un contenedor de docker:

    $ docker run --name some-mongo -p 27017:27017 -d mongo

### Enlaces de referencia ###

- Diapositivas CáceresJS 13 Marzo 2018: https://docs.google.com/presentation/d/1SoQZfoAqdv_nA9IIOHtfJ6oeCk1oKtcR9oRMCf0_NpI/edit?usp=sharing
- Charla Asincronía Codemotion 2018: https://www.youtube.com/watch?v=wsO2-9zzwLg&list=PLKxa4AIfm4pWeEgciQ_898Tqzsd24lApG&index=27
- Charla Rx.js T3chFest 2018: https://t3chfest.uc3m.es/2018/programa/entendamos-la-reactividad/
- https://blog.sessionstack.com/how-does-javascript-actually-work-part-1-b0bacc073cf
- https://medium.com/@gaurav.pandvia/understanding-javascript-function-executions-tasks-event-loop-call-stack-more-part-1-5683dea1f5ec
- http://thecodebarbarian.com/2015/03/20/callback-hell-is-a-myth
- https://medium.com/@mlfryman/to-survive-the-pyramid-of-doom-4e8ce4fb5d6b
- http://lemoncode.net/lemoncode-blog/2018/1/29/javascript-asincrono
- https://medium.com/@hidace/javascript-es6-generators-part-i-understanding-generators-93dea22bf1b
- http://www.foaas.com/
- https://caniuse.com/#search=async%20functions