'use strict';

const MongoClient = require('mongodb').MongoClient;
const mongoEndpoint = 'mongodb://192.168.99.100/';
const TicketProduct = require('./models/ticketproduct.js');

let ticketTotal = 0;
const products = [
    new TicketProduct('ticket1', 'Fries whith Bacon and Cheese', '1', '8.60'),
    new TicketProduct('ticket1', 'Diablo Burger', '1', '10.30'),
    new TicketProduct('ticket1', 'Heart attack Burger', '1', '13.90'),
    new TicketProduct('ticket1', 'Pepsi Max', '2', '1.50'),
    new TicketProduct('ticket1', 'Gelato di Fragole', '1', '3.20'),
    new TicketProduct('ticket1', 'Freak Shake', '1', '5.45')
];

function* generator(collection) {
    for (let product of products) {
        console.log(product.name);
        ticketTotal += product.cost();
        yield collection.insertOne(product);
    }
    return;
}

( async function() {

    const client = await MongoClient.connect(mongoEndpoint, {useNewUrlParser: true});
    const db = client.db("caceresjs");
    const collection = db.collection('ticket_product');

    collection.deleteMany({})
        .then(function() {

            const g = generator(collection);

            iterateProduct();

            function iterateProduct() {
                let nextProduct = g.next();
                if (!nextProduct.done) {
                    nextProduct.value.then(function(result) {
                        console.log(result.result);
                        iterateProduct();
                    });
                } else {
                    console.log("Ticket Total: " + ticketTotal);
                    client.close();
                }
            }


        });
})();