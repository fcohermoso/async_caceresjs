'use strict';

const MongoClient = require('mongodb').MongoClient;
const mongoEndpoint = 'mongodb://192.168.99.100/';
const TicketProduct = require('./models/ticketproduct.js');

let ticketTotal = 0;
const entry = new TicketProduct('ticket1', 'Fries whith Bacon and Cheese', '1', '8.60');
const firstBurger = new TicketProduct('ticket1', 'Diablo Burger', '1', '10.30');
const secondBurger = new TicketProduct('ticket1', 'Heart attack Burger', '1', '13.90');
const drinks = new TicketProduct('ticket1', 'Pepsi Max', '2', '1.50');
const dessert = new TicketProduct('ticket1', 'Gelato di Fragole', '1', '3.20');
const dessert2 = new TicketProduct('ticket1', 'Freak Shake', '1', '5.45');

( async function() {

    const client = await MongoClient.connect(mongoEndpoint, {useNewUrlParser: true});
    const db = client.db("caceresjs");
    const collection = db.collection('ticket_product');

    collection.deleteMany({}).then(function() {
        insertProducts(client, collection);
    });

})();

async function insertProducts(client, collection) {

    let insertOnePromise;

    console.log(entry.name);
    ticketTotal += entry.cost();
    insertOnePromise = await collection.insertOne(entry);
    console.log(insertOnePromise.result);

    console.log(firstBurger.name);
    ticketTotal += firstBurger.cost();
    insertOnePromise = await collection.insertOne(firstBurger);
    console.log(insertOnePromise.result);

    console.log(secondBurger.name);
    ticketTotal += secondBurger.cost();
    insertOnePromise = await collection.insertOne(secondBurger);
    console.log(insertOnePromise.result);

    console.log(drinks.name);
    ticketTotal += drinks.cost();
    insertOnePromise = await collection.insertOne(drinks);
    console.log(insertOnePromise.result);

    console.log(dessert.name);
    ticketTotal += dessert.cost();
    insertOnePromise = await collection.insertOne(dessert);
    console.log(insertOnePromise.result);

    console.log(dessert2.name);
    ticketTotal += dessert2.cost();
    insertOnePromise = await collection.insertOne(dessert2);
    console.log(insertOnePromise.result);

    console.log("Ticket Total: " + ticketTotal);
    client.close();
}