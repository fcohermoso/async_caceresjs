'use strict';

const MongoClient = require('mongodb').MongoClient;
const mongoEndpoint = 'mongodb://192.168.99.100/';
const TicketProduct = require('./models/ticketproduct.js');

let ticketTotal = 0;
const entry = new TicketProduct('ticket1', 'Fries whith Bacon and Cheese', '1', '8.60');
const firstBurger = new TicketProduct('ticket1', 'Diablo Burger', '1', '10.30');
const secondBurger = new TicketProduct('ticket1', 'Heart attack Burger', '1', '13.90');
const drinks = new TicketProduct('ticket1', 'Pepsi Max', '2', '1.50');
const dessert = new TicketProduct('ticket1', 'Gelato di Fragole', '1', '3.20');
const dessert2 = new TicketProduct('ticket1', 'Freak Shake', '1', '5.45');

( async function() {

    const client = await MongoClient.connect(mongoEndpoint, {useNewUrlParser: true});
    const db = client.db("caceresjs");
    const collection = db.collection('ticket_product');

    collection.deleteMany({}, function() {
        try {
            collection.insertOne(entry, function() {
                ticketTotal += entry.cost();
                collection.insertOne(firstBurger, function() {
                    ticketTotal += firstBurger.cost();
                    collection.insertOne(secondBurger, function() {
                        ticketTotal += secondBurger.cost();
                        collection.insertOne(drinks, function() {
                            ticketTotal += drinks.cost();
                            collection.insertOne(dessert, function() {
                                ticketTotal += dessert.cost();
                                collection.insertOne(dessert2, function() {
                                    ticketTotal += dessert2.cost();
                                    console.log("Ticket Total: " + ticketTotal);
                                    client.close();
                                });
                            });
                        });
                    });
                });
            });
        } catch (error) {
            console.log(error);
        }
    });


})();