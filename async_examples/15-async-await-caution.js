'use strict';

const axios = require("axios");
const microprofiler = require('microprofiler');

function getValueA() {
    return axios.get("http://www.foaas.com/dalton/Async-Await/Kiko");
}

function getValueB() {
    return axios.get("http://www.foaas.com/cool/Kiko");
}

function getValueC() {
    return axios.get("http://www.foaas.com/too/Kiko");
}

async function asyncAwaitCaution() {
    const start = microprofiler.start();
    let A = await getValueA();
    let B = await getValueB();
    let C = await getValueC();

    console.log("=== Wrong way");
    console.log(A.data.message);
    console.log(B.data.message);
    console.log(C.data.message);

    microprofiler.measureFrom(start, 'asyncAwaitCaution')
    microprofiler.show('asyncAwaitCaution');
}

async function asyncAwaitPromiseAll() {
    const start = microprofiler.start();
    let A = getValueA();
    let B = getValueB();
    let C = getValueC();

    // Promise.all() allows us to send all requests at the same time. 
    let results = await Promise.all([A, B, C]);

    console.log("=== Promise.all()");
    console.log(results[0].data.message);
    console.log(results[1].data.message);
    console.log(results[2].data.message);

    microprofiler.measureFrom(start, 'asyncAwaitPromiseAll');
    microprofiler.show('asyncAwaitPromiseAll');
}

asyncAwaitCaution();
asyncAwaitPromiseAll();