'use strict';

const request = require('request');

const options = {
    url: "http://www.foaas.com/what/Kiko",
    headers: {
        "Accept": "application/json"
    }
};

const callback = function(error, response, body) {
    console.log(body);
}

console.log("Before");
request(options, callback);
console.log("After");

console.log("Before setTimeout");
setTimeout(function() {
    console.log("setTimeout Callback 500");
}, 500);

setTimeout(function() {
    console.log("setTimeout Callback 0");
}, 0);
console.log("After setTimeout");