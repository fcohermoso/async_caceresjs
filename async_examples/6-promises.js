'use strict';

const promise = new Promise(function(resolve, reject) {
    let randomNumber = Math.round(Math.random() * 2000);

    if (randomNumber > 1000) {
        resolve(randomNumber);
    } else {
        reject(new Error(randomNumber));
    }
});

promise.then(function(success) {
    console.log("OK: " + success);
}).catch(function(error) {
    console.log("KO: " + error.message);
});