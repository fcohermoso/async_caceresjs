'use strict';

function* generator() {
    yield 'In';
    console.log("yield 1");
    yield 'local';
    console.log("yield 2");
    yield 'works';
    console.log("yield 3");
    return;
}

const g = generator();
let done = false;

while (!done) {
    const next = g.next();
    done = next.done;
    console.log(next);
}