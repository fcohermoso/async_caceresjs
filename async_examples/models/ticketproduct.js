'use strict';

function TicketProduct(ticketId, name, quantity, price) {
    this.ticketId = ticketId;
    this.name = name;
    this.quantity = parseInt(quantity);
    this.price = parseFloat(price);
}

TicketProduct.prototype.cost = function() {
    return this.quantity * this.price;
};

module.exports = TicketProduct;