'use strict';

const MongoClient = require('mongodb').MongoClient;
const mongoEndpoint = 'mongodb://192.168.99.100/';
const TicketProduct = require('./models/ticketproduct.js');

let ticketTotal = 0;
const entry = new TicketProduct('ticket1', 'Fries whith Bacon and Cheese', '1', '8.60');
const firstBurger = new TicketProduct('ticket1', 'Diablo Burger', '1', '10.30');
const secondBurger = new TicketProduct('ticket1', 'Heart attack Burger', '1', '13.90');
const drinks = new TicketProduct('ticket1', 'Pepsi Max', '2', '1.50');
const dessert = new TicketProduct('ticket1', 'Gelato di Fragole', '1', '3.20');
const dessert2 = new TicketProduct('ticket1', 'Freak Shake', '1', '5.45');

( async function() {

    const client = await MongoClient.connect(mongoEndpoint, {useNewUrlParser: true});
    const db = client.db("caceresjs");
    const collection = db.collection('ticket_product');

    collection.deleteMany({}, function() {
        console.log(entry.name);
        collection.insertOne(entry, function(error, result) {
            console.log(result.result);
            ticketTotal += entry.cost();
            console.log(firstBurger.name);
            collection.insertOne(firstBurger, function(error, result) {
                console.log(result.result);
                ticketTotal += firstBurger.cost();
                console.log(secondBurger.name);
                collection.insertOne(secondBurger, function(error, result) {
                    console.log(result.result);
                    ticketTotal += secondBurger.cost();
                    console.log(drinks.name);
                    collection.insertOne(drinks, function(error, result) {
                        console.log(result.result);
                        ticketTotal += drinks.cost();
                        console.log(dessert.name);
                        collection.insertOne(dessert, function(error, result) {
                            console.log(result.result);
                            ticketTotal += dessert.cost();
                            console.log(dessert2.name);
                            collection.insertOne(dessert2, function(error, result) {
                                console.log(result.result);
                                ticketTotal += dessert2.cost();
                                console.log("Ticket Total: " + ticketTotal);
                                client.close();
                            });
                        });
                    });
                });
            });
        });
    });


})();